import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        int[] myArray = {5,2,4,6,1,3,2,6};
        System.out.println("Сортировка слиянием через рерурсию");
        System.out.print("до:    ");
        System.out.println(Arrays.toString(myArray));
        int[] myArrayAfter = mergeSort(myArray, 0, myArray.length - 1);
        System.out.print("после: ");
        System.out.println(Arrays.toString(myArrayAfter));
    }

    public static int[] mergeSort(int[] arr, int begin, int end) {
        int split;
        int[] b;
        if (begin < end) {
            split = (begin + end) / 2;
            mergeSort(arr, begin, split);
            mergeSort(arr, split + 1, end);
            b = merge(arr, begin, split, end);
            return b;
        } else return arr;
    }

    static int[] merge(int[] arr, int begin, int mid, int end) {
        int pos1 = begin;
        int pos2 = mid + 1;
        int pos3 = 0;
        int[] temp = new int[end - begin + 1];
        while (pos1 <= mid && pos2 <= end) {
            if (arr[pos1] < arr[pos2]) {
                temp[pos3++] = arr[pos1++];
            } else {
                temp[pos3++] = arr[pos2++];
            }
        }
        while (pos2 <= end) {
            temp[pos3++] = arr[pos2++];
        }
        while (pos1 <= mid) {
            temp[pos3++] = arr[pos1++];
        }

        for (pos3 = 0; pos3 < end - begin + 1; pos3++) {
            arr[begin + pos3] = temp[pos3];
        }
        return arr;
    }
}